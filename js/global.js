function createIsometricMap(){
  var grid = tileGrid;
  var mapWidth = tileSize;
  var mapHeight = tileSize;
  // var centerX = (-grid.length / 0.75) * tileWidthHalf;
  // var centerY = -300;
  var centerX = -gameWidth * 2.2;
  var centerY = -gameHeight * 1.5;
  for (var y = 0; y < grid.length; y++){
     for (var x = 0; x < grid[y].length; x++){
      var tx = (x - y) * tileWidthHalf;
      var ty = (x + y) * tileHeightHalf;
      var tileName = grid[y][x].toString();
      if (tileName.length < 3) tileName = '0' + tileName;
      var block = 'block-' + tileName;
      var tile = Game.add.image(centerX + tx, centerY + ty, 'isoblocks', block);
      tile.setInteractive();
      tile.setData('row', x);
      tile.setData('col', y);
      tile.setDepth(centerY + ty);
      blocks.push(tile);
    }
  }
  blocks.forEach(function (block) {
    block.on('pointerover', function(pointer, x, y){
      if (selectionMode){
        this.y -= selectedHeight;
        this.setTint(gameTints.selected);
        selectedTile = this;
      }
    });
    block.on('pointerout', function(pointer, x, y){
      if (selectionMode){
        this.y += selectedHeight;
        this.clearTint();
        selectedTile = this;
      }
    })
  });
  // Pathfinder Set-Up
  Game.pathFinder = easyStar;
  Game.pathFinder.setGrid(tileGrid);
  Game.pathFinder.setAcceptableTiles([14]);
}


function convertWorldToTile(worldX, worldY) {
  worldX = worldX.toFixed();
  worldY = worldY.toFixed();
  var centerX = -gameWidth * 2.2;
  var centerY = -gameHeight * 1.5;
  return {
    x: parseInt(Math.abs((centerX / tileWidthHalf + centerX / tileHeightHalf) / 2).toFixed()),
    y: parseInt(Math.abs((centerY/ tileHeightHalf -(centerY / tileWidthHalf)) / 2).toFixed())
  };
}

function createMarker(){
  var blockA = blocks[340];
  var blockB = blocks[360];
  blockB.setTint(gameTints.selected);
  marker = Game.add.image(blockA.x, blockA.y, 'marker');
  marker.setScale(.7, .7);
  Game.marker = marker;

  var fromX = convertWorldToTile(blockA.x, blockA.y).x;
  var fromY = convertWorldToTile(blockA.x, blockA.y).y;
  var toX = convertWorldToTile(blockB.x, blockB.y).x;
  var toY = convertWorldToTile(blockB.x, blockB.y).y;
  console.log(fromX);
  console.log(fromY);
  console.log(toX);
  console.log(toY);
  // Game.pathFinder.findPath(fromX, fromY, toX, toY, function( path ) {
  //     if (path === null) {
  //         console.warn("Path Not Found.");
  //     } else {
  //         console.log(path);
  //     }
  // });
  // Game.pathFinder.calculate();
}

function mapZoomHandler(){
  Game.input.keyboard.on('keydown_A', function (event) {
    if (gameCamera.zoom < 1.5) gameCamera.zoom += zoomFactor;
  });
  Game.input.keyboard.on('keydown_S', function (event) {
    if (gameCamera.zoom > 1){
      gameCamera.zoom -= zoomFactor;
    } else {
      gameCamera.zoom = 1;
    }
  });
}
