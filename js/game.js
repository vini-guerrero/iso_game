var config = {
  type: Phaser.WEBGL,
  width: gameWidth,
  height: gameHeight,
  backgroundColor: '#0d0d0d',
  parent: 'phaser-game',
  scene: { preload: preload, create: create, update: update }
};
var game = new Phaser.Game(config);

function preload (){
  this.load.atlas('isoblocks', 'assets/isoblocks.png', 'assets/isoblocks.json');
  this.load.image('bg', 'assets/background.jpg');
  this.load.image('marker', 'assets/orb.png');
}

function create (){
  Game = this;
  // this.add.image(0, 0, 'bg').setOrigin(0);
  debug = this.add.text(10, 10, debugText);
  var frames = this.textures.get('isoblocks').getFrameNames();
  createIsometricMap();
  var cursors = this.input.keyboard.createCursorKeys();
  var controlConfig = {
    camera: this.cameras.main,
    left: cursors.left,
    right: cursors.right,
    up: cursors.up,
    down: cursors.down,
    acceleration: 0.04,
    drag: 0.0005,
    maxSpeed: 1
  };
  controls = new Phaser.Cameras.Controls.SmoothedKeyControl(controlConfig);
  // this.cameras.main.setBounds(0, 0, 3300, 3300);
  // this.cameras.main.zoom = 1;
  createMarker();
}


function update (time, delta){
  mouse = Game.input.activePointer;
  gameCamera = this.cameras.main;
  debugText = "Mouse  Position  X:" + mouse.position.x + " Y:" + mouse.position.y + "\n";
  if (selectedTile) {
    var selectedTileX = convertWorldToTile(selectedTile.x, selectedTile.y).x;
    var selectedTileY = convertWorldToTile(selectedTile.y, selectedTile.y).y;
    debugText += "Tile   Position  X:" + selectedTileX + " Y:" + selectedTileY + "\n";
  }
  if (Game.marker){
    var marker = Game.marker;
    var markerTileX = convertWorldToTile(marker.x, marker.y).x;
    var markerTileY = convertWorldToTile(marker.x, marker.y).y;
    debugText += "Marker Position  X:" + markerTileX + " Y:" + markerTileY + "\n";
  }
  debugText += "Main Camera Zoom: " + gameCamera.zoom + "\n";
  debug.setText(debugText);
  debug.x = gameCamera.scrollX + 20;
  debug.y = gameCamera.scrollY + 20;
  controls.update(delta);
  gameCamera.scrollX = mouse.x - 3900;
  gameCamera.scrollY = mouse.y - 1200;
  mapZoomHandler();
}
