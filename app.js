var express = require('express');
var app = express();
var server = require('http').Server(app);
var defaultPort = 8080;

app.use('/vendor', express.static(__dirname + '/vendor'));
app.use('/js', express.static(__dirname + '/js'));
app.use('/css', express.static(__dirname + '/css'));
app.use('/assets', express.static(__dirname + '/assets'));
app.use('/maps', express.static(__dirname + '/maps'));
app.use('/tiles', express.static(__dirname + '/tiles'));

// Game Client
app.get('/', function(req, res) {
  res.sendFile(__dirname + '/index.html');
});

// Server Handling
server.listen(process.env.PORT || defaultPort, function() {
  console.log('Listening on ' + server.address().port);
});
